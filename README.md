# Kata TDD #

Kata para ejercicio de TDD basada en la propuesta por Roy Osherove, http://osherove.com/tdd-kata-1/ .

# Objetivo #

Implementar una calculadora de Strings donde sume los valores ingresados

# Implementación #

Los pasos con sus respectivas pruebas realizadas fueron los siguientes:

# Paso 1 #

- Ingresar un string vacio

# Paso 2 #

- Ingresar un string con un número

# Paso 3 #

- Ingresar un string con dos números

# Paso 4 #

Pruebas al ingresar mas de 2 números en la cadena

- Ingresar 3 números diferentes

- Ingresar 4 números diferentes

- Ingresar 3 números diferentes y 1 letra

# Paso 5 #

Pruebas al ingresar con otros separadores ( : y &)

- Ingresar 2 números separados por ":"

- Ingresar 3 números separados por "&"

- Ingresar 4 números separados por ":" y "&"