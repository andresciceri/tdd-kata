from unittest import TestCase
from Calculadora import Calculadora

__author__ = 'andresciceri'


class TestCalculadora(TestCase):
  def test_calcular(self):
    s = Calculadora()
    resultado = s.calcular("")
    self.assertEquals(0,resultado,"El metodo no esta funcionando, no devuelve cero")

  def test_calcular_un_numero(self):
    s = Calculadora()
    dato = "1"
    resultado = s.calcular(dato)
    self.assertEquals(dato,resultado,"El metodo no esta funcionando, no devuelve el numero ingresado")

  def test_calcular_dos_numero(self):
    s = Calculadora()
    dato = "2,5"
    resultado = s.calcular(dato)
    self.assertEquals(7,resultado,"El metodo no esta funcionando, no devuelve la suma de los dos numeros")

  def test_calcular_n_numeros(self):
      s = Calculadora()
      dato_1 = "2,3,5"
      resultado = s.calcular(dato_1)
      self.assertEquals(10,resultado,"El metodo no esta funcionando, no devuelve la suma correcta de los numeros en la cadena")

      dato_2 = "2,3,5,7"
      resultado = s.calcular(dato_2)
      self.assertEquals(17,resultado,"El metodo no esta funcionando, no devuelve la suma correcta de los numeros en la cadena")

      dato_3 = "2,3,5,h"
      resultado = s.calcular(dato_3)
      self.assertEquals(10,resultado,"El metodo no esta funcionando, no devuelve la suma correcta de los numeros en la cadena")

  def test_calcular_otro_separador(self):
      s = Calculadora()
      dato_1 = "5:7"
      resultado = s.calcular(dato_1)
      self.assertEquals(12,resultado,"El metodo no esta funcionando, no devuelve el valor correcto de la suma de los numeros")

      dato_1 = "5&7&4"
      resultado = s.calcular(dato_1)
      self.assertEquals(16,resultado,"El metodo no esta funcionando, no devuelve el valor correcto de la suma de los numeros")

      dato_1 = "5&7:4&9:3"
      resultado = s.calcular(dato_1)
      self.assertEquals(28,resultado,"El metodo no esta funcionando, no devuelve el valor correcto de la suma de los numeros")