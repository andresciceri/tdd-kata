import re
class Calculadora:
    def calcular(self, numero):
        numeroArr = re.split('[, : &]',numero)

        if numero == "":
            return 0
        elif len(numeroArr) == 1:
            return numero
        elif len(numeroArr) == 2:
            num_uno = int(numeroArr[0])
            num_dos = int(numeroArr[1])
            return num_uno + num_dos
        elif len(numeroArr) > 2:
            result = 0;
            for num in numeroArr:
                if num.isdigit():
                    result += int(num)
            return result
